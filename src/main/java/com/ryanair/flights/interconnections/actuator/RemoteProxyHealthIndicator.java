package com.ryanair.flights.interconnections.actuator;

import com.ryanair.flights.interconnections.adapters.BaseAdapter;
import com.ryanair.flights.interconnections.exception.HealthException;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;

public class RemoteProxyHealthIndicator implements HealthIndicator {
  private final BaseAdapter proxyAdapter;
  private final String healthUrl;

  public RemoteProxyHealthIndicator(BaseAdapter proxyAdapter,
                                    String healthUrl) {
    this.proxyAdapter = proxyAdapter;
    this.healthUrl = healthUrl;
  }

  @Override
  public Health health() {
    try {
      proxyAdapter.health();
      return Health.up().withDetail(healthUrl, "UP").build();
    } catch (HealthException e) {
      return Health.down().withDetail(healthUrl, "DOWN").withException(e).build();
    }
  }
}
