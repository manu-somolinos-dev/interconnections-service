package com.ryanair.flights.interconnections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InterconnectionsServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(InterconnectionsServiceApplication.class, args);
	}

}


