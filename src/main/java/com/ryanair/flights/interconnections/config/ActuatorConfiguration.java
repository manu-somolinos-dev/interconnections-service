package com.ryanair.flights.interconnections.config;

import com.ryanair.flights.interconnections.actuator.RemoteProxyHealthIndicator;
import com.ryanair.flights.interconnections.adapters.RouteAdapter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ActuatorConfiguration {

  @Bean("RouteHealthIndicator")
  public RemoteProxyHealthIndicator routeHealthIndicator(RouteAdapter routeAdapter,
      @Value("${interconnections.proxy.adapter.routes.base-url}") String baseUrl,
      @Value("${interconnections.proxy.adapter.routes.health}") String health) {
    return new RemoteProxyHealthIndicator(routeAdapter, baseUrl + health);
  }

  @Bean("ScheduleHealthIndicator")
  public RemoteProxyHealthIndicator scheduleHealthIndicator(RouteAdapter routeAdapter,
      @Value("${interconnections.proxy.adapter.schedules.base-url}") String baseUrl,
      @Value("${interconnections.proxy.adapter.schedules.health}") String health) {
    return new RemoteProxyHealthIndicator(routeAdapter, baseUrl + health);
  }

}
