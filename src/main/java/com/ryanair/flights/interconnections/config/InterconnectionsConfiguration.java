package com.ryanair.flights.interconnections.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.datetime.standard.DateTimeFormatterRegistrar;
import org.springframework.format.support.DefaultFormattingConversionService;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.time.format.DateTimeFormatter;

import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;
import static java.time.temporal.ChronoUnit.MILLIS;

@Configuration
@EnableCaching
public class InterconnectionsConfiguration {

  @Bean
  public ObjectMapper objectMapper() {
    return new Jackson2ObjectMapperBuilder()
        .modules(new JavaTimeModule())
        .featuresToDisable(WRITE_DATES_AS_TIMESTAMPS)
        .build();
  }

  @Bean
  public RestTemplate restTemplate(@Value("${interconections.proxy.connection-timeout:5000}") long connectionTimeout,
                                   @Value("${interconnections.proxy.read-tiemout:10000}") long readTimeout) {
    return new RestTemplateBuilder()
        .setConnectTimeout(Duration.of(connectionTimeout, MILLIS))
        .setReadTimeout(Duration.of(readTimeout, MILLIS))
        .build();
  }


  @Bean
  public FormattingConversionService conversionService() {
    DefaultFormattingConversionService conversionService = new DefaultFormattingConversionService(false);

    DateTimeFormatterRegistrar registrar = new DateTimeFormatterRegistrar();
    registrar.setDateTimeFormatter(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm"));
    registrar.registerFormatters(conversionService);
    return conversionService;
  }

}
