package com.ryanair.flights.interconnections.controller;

import com.ryanair.flights.interconnections.domain.ErrorMessage;
import com.ryanair.flights.interconnections.exception.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolationException;

import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@ControllerAdvice
@Order(HIGHEST_PRECEDENCE)
@Slf4j
public class ExceptionHandlerController {

  @ExceptionHandler({ConstraintViolationException.class})
  @ResponseStatus(BAD_REQUEST)
  public ResponseEntity<ErrorMessage> handleConstraintViolationException(WebRequest request,
                                                                         ConstraintViolationException e) {
    log.error("Bad Request Exception: {}", e.getMessage());
    return ResponseEntity.badRequest().body(new ErrorMessage(BAD_REQUEST.getReasonPhrase(), "Invalid Input Parameters"));
  }

  @ExceptionHandler({MethodArgumentTypeMismatchException.class})
  @ResponseStatus(BAD_REQUEST)
  public ResponseEntity<ErrorMessage> handleMethodArgumentTypeMismatchException(WebRequest request,
                                                                MethodArgumentTypeMismatchException e) {
    log.error("Bad Request Exception: {}", e.getMessage());
    return ResponseEntity.badRequest().body(new ErrorMessage(BAD_REQUEST.getReasonPhrase(), e.getMessage()));
  }

  @ExceptionHandler({NotFoundException.class})
  @ResponseStatus(NOT_FOUND)
  public ResponseEntity<ErrorMessage> handleNotFoundException(WebRequest request,
                                                              NotFoundException e) {
    log.error("Not Found Exception: {}", e.getMessage());
    return ResponseEntity.status(NOT_FOUND).body(new ErrorMessage(NOT_FOUND.getReasonPhrase(), e.getMessage()));
  }

  @ExceptionHandler({RuntimeException.class})
  @ResponseStatus(INTERNAL_SERVER_ERROR)
  public ResponseEntity<ErrorMessage> handleOutstandingRuntimeException(WebRequest request,
                                                                        RuntimeException e) {
    log.error("Internal Server Error Exception: {}", e.getMessage());
    return ResponseEntity.status(INTERNAL_SERVER_ERROR)
        .body(new ErrorMessage(INTERNAL_SERVER_ERROR.getReasonPhrase(), e.getMessage()));
  }
}
