package com.ryanair.flights.interconnections.controller;

import com.ryanair.flights.interconnections.domain.Interconnection;
import com.ryanair.flights.interconnections.exception.NotFoundException;
import com.ryanair.flights.interconnections.services.InterconnectionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.List;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

/**
 * Controller bean with all available endpoints related to Interconnections domain entity
 */
@Api(value="Flight Interconnections Search Engine")
@RestController
@RequestMapping("/interconnections")
@Validated
@Slf4j
public class InterconnectionsController {

  private final InterconnectionService interconnectionService;

  @Autowired
  public InterconnectionsController(InterconnectionService interconnectionService) {
    this.interconnectionService = interconnectionService;
  }

  @ApiOperation(value = "Flight Interconnections list", response = List.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully retrieved list"),
      @ApiResponse(code = 400, message = "Bad Request: Bad Input criteria"),
      @ApiResponse(code = 404, message = "Not Found: Unable to find any Flight Interconnection with input Criteria")
  })
  @GetMapping(produces = APPLICATION_JSON_UTF8_VALUE)
  @ResponseStatus(OK)
  public @ResponseBody List<Interconnection> get(
      @ApiParam(value = "Departure airport valid IATA code", required = true)
        @NotBlank @RequestParam String departure,
      @ApiParam(value = "Arrival airport valid IATA code", required = true)
        @NotBlank @RequestParam String arrival,
      @ApiParam(value = "Departure date (format:yyyy-MM-dd'T'HH:mm)", required = true)
        @Valid @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm") LocalDateTime departureDateTime,
      @ApiParam(value = "Arrival date (format:yyyy-MM-dd'T'HH:mm)", required = true)
        @Valid @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm") LocalDateTime arrivalDateTime) {
    log.info("get-interconnections['departure':'{}','arrival':'{}', 'departureDateTime':'{}', 'arrivalDateTime':'{}']",
        departure,
        arrival,
        departureDateTime,
        arrivalDateTime
    );
    List<Interconnection> interconnections = interconnectionService.findAll(departure, arrival, departureDateTime, arrivalDateTime);
    if (interconnections.isEmpty()) {
      throw new NotFoundException("Unable to find any interconnection with Input Criteria");
    }
    return interconnections;
  }
}
