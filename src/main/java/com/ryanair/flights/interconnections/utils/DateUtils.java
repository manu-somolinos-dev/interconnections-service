package com.ryanair.flights.interconnections.utils;

import java.time.LocalDateTime;
import java.time.LocalTime;

public final class DateUtils {
  private DateUtils() {
    // N/A
  }

  public static LocalDateTime buildLocalDateTime(LocalDateTime localDateTime, LocalTime localTime) {
    return localDateTime
        .withHour(localTime.getHour())
        .withMinute(localTime.getMinute())
        .withSecond(0)
        .withNano(0);
  }
}
