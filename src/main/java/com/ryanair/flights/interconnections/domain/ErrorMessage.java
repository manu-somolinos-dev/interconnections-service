package com.ryanair.flights.interconnections.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@ApiModel(description = "Exception Domain Entity")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorMessage {
  @ApiModelProperty(notes = "Standard Code")
  private String code;
  @ApiModelProperty(notes = "Description Message")
  private String message;
}
