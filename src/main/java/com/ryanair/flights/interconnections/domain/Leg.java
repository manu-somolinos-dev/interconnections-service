package com.ryanair.flights.interconnections.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@ApiModel(description = "Flight Detail Domain Entity")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Leg {
  @ApiModelProperty(notes = "Flight Departure Airport")
  private String departureAirport;
  @ApiModelProperty(notes = "Flight Arrival Airport")
  private String arrivalAirport;
  @ApiModelProperty(notes = "Flight Departure Date Airport")
  private LocalDateTime departureDateTime;
  @ApiModelProperty(notes = "Flight Arrival Date Airport")
  private LocalDateTime arrivalDateTime;
}
