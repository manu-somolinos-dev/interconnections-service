package com.ryanair.flights.interconnections.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;


@ApiModel(description = "Flight Interconnection Domain Entity")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Interconnection {

  @ApiModelProperty(notes = "Interconnection intermediate flights number")
  private int stops;
  @ApiModelProperty(notes = "Flight list")
  private List<Leg> legs = new ArrayList<>();
}
