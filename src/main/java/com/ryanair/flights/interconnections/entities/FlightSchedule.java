package com.ryanair.flights.interconnections.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FlightSchedule {
  private String carrierCode;
  private String number;
  private LocalTime departureTime;
  private LocalTime arrivalTime;
}
