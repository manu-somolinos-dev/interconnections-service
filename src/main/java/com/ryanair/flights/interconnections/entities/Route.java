package com.ryanair.flights.interconnections.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Route {
  private String airportFrom;
  private String airportTo;
  private String connectingAirport;
  private boolean newRoute;
  private boolean seasonalRoute;
  private String operator;
  private String group;
  private List<String> similarArrivalAirportCodes = new ArrayList<>();
  private List<String> tags = new ArrayList<>();
  private String carrierCode;
}
