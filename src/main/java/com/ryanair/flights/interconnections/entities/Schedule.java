package com.ryanair.flights.interconnections.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Schedule {
  private int month;
  private List<DaySchedule> days = new ArrayList<>();
}
