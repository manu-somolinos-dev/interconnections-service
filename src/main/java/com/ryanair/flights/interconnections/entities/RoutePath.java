package com.ryanair.flights.interconnections.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoutePath {
  private String departure;
  private String arrival;
  private List<String> connections = new ArrayList<>();

  public RoutePath(List<String> airports) {
    this(airports.get(0), airports.get(airports.size() - 1), airports);
  }

  public int getStops() {
    return connections.size() - 2;
  }
}
