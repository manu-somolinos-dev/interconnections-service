package com.ryanair.flights.interconnections.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DaySchedule {
  private int day;
  private List<FlightSchedule> flights = new ArrayList<>();
}
