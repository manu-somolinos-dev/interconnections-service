package com.ryanair.flights.interconnections.exception;

public class HealthException extends RuntimeException {
  public HealthException(String message) {
    super(message);
  }
}
