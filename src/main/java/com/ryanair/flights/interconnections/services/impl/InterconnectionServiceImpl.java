package com.ryanair.flights.interconnections.services.impl;

import com.ryanair.flights.interconnections.domain.Interconnection;
import com.ryanair.flights.interconnections.domain.Leg;
import com.ryanair.flights.interconnections.entities.FlightSchedule;
import com.ryanair.flights.interconnections.entities.RoutePath;
import com.ryanair.flights.interconnections.exception.BadRequestException;
import com.ryanair.flights.interconnections.services.InterconnectionService;
import com.ryanair.flights.interconnections.services.RoutePathService;
import com.ryanair.flights.interconnections.services.ScheduleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.ryanair.flights.interconnections.utils.DateUtils.buildLocalDateTime;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.mapping;

@Service
@Slf4j
public class InterconnectionServiceImpl implements InterconnectionService {

  private final RoutePathService routePathService;

  private final ScheduleService scheduleService;

  private final int maxConnectionStops;

  private final int minHoursBetweenInterconnections;

  @Autowired
  public InterconnectionServiceImpl(RoutePathService routePathService,
                                    ScheduleService scheduleService,
                                    @Value("${interconnections.constants.max-connection-stops}") int maxConnectionStops,
                                    @Value("${interconnections.constants.min-hours-between-interconnection}") int minHoursBetweenInterconnections) {
    this.routePathService = routePathService;
    this.scheduleService = scheduleService;
    this.maxConnectionStops = maxConnectionStops;
    this.minHoursBetweenInterconnections = minHoursBetweenInterconnections;
  }

  @Override
  public List<Interconnection> findAll(String departure, String arrival, LocalDateTime departureDateTime, LocalDateTime arrivalDateTime) {
    if (departureDateTime.isAfter(arrivalDateTime)) {
      String errorMessage = "Departure DateTime cannot be lower than Arrival DateTime";
      log.error(errorMessage);
      throw new BadRequestException(errorMessage);
    }

    // find interconected flights with one stop route
    log.info("get all available route-paths for [{} - {}] - with maximum {} stop(s)", departure, arrival, maxConnectionStops);
    List<RoutePath> routes = routePathService.findAll(departure, arrival).stream()
        .filter(routePath -> routePath.getStops() <= maxConnectionStops)
        .collect(Collectors.toList());

    log.info("get all possible interconnections");
    return routes.parallelStream()
        .map(routePath -> {
          String origin = routePath.getConnections().get(0);
          List<String> pendingPath = routePath.getConnections().subList(1, routePath.getConnections().size());
          return this.findInterconnectionPath(origin, pendingPath, departureDateTime, arrivalDateTime, new Interconnection());
        })
        .filter(Optional::isPresent)
        .collect(mapping(Optional::get, Collectors.toList()));
  }

  /**
   * find a valid flight schedule from the departure airport to the next steps collected in pending path bounded by
   * departure and arrival dates. It accumulates results in accumInterconnection param.
   *
   * @param departure
   * @param pendingPath
   * @param departureDateTime
   * @param arrivalDateTime
   * @param accumInterconnection
   * @return
   */
  private Optional<Interconnection> findInterconnectionPath(String departure,
                                                            List<String> pendingPath,
                                                            LocalDateTime departureDateTime,
                                                            LocalDateTime arrivalDateTime,
                                                            Interconnection accumInterconnection) {
    if (pendingPath.isEmpty()) {
      return ofNullable(accumInterconnection);
    } else {
      String arrival = pendingPath.get(0);
      // Find Schedule
      Optional<FlightSchedule> flightSchedule = scheduleService.findFlightSchedule(departure, arrival, departureDateTime, arrivalDateTime);
      if (flightSchedule.isPresent()) {
        // Create new leg with flight schedule found
        LocalDateTime legDepartureDateTime = buildLocalDateTime(departureDateTime, flightSchedule.get().getDepartureTime());
        LocalDateTime legArrivalDateTime = buildLocalDateTime(departureDateTime, flightSchedule.get().getArrivalTime());
        if (flightSchedule.get().getArrivalTime().isBefore(flightSchedule.get().getDepartureTime())) {
          legArrivalDateTime = legArrivalDateTime.plusDays(1);
        }
        log.debug("Flight scheduled [{} -> {}]", departure, arrival);
        Leg newLeg = new Leg(departure, arrival, legDepartureDateTime, legArrivalDateTime);
        // push new leg to Interconnection accumulator
        accumInterconnection.getLegs().add(newLeg);
        accumInterconnection.setStops(accumInterconnection.getLegs().size() - 1);
        // prepare input params for next step in flight pending path
        String nextDeparture = pendingPath.get(0);
        List<String> nextPendingPath = pendingPath.subList(1, pendingPath.size());
        LocalDateTime nextDepartureDate = legArrivalDateTime.plusHours(minHoursBetweenInterconnections);
        return findInterconnectionPath(nextDeparture, nextPendingPath, nextDepartureDate, arrivalDateTime, accumInterconnection);
      } else {
        // Not found schedule -> time to leave this route-path
        log.debug("Aborted flight interconnection - Impossible [{} -> {}] flight on time schedule", departure, arrival);
        return findInterconnectionPath(null, Collections.emptyList(), null, null, null);
      }
    }
  }

}
