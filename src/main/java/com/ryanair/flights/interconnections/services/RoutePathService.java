package com.ryanair.flights.interconnections.services;

import com.ryanair.flights.interconnections.entities.RoutePath;

import java.util.List;

public interface RoutePathService {
  List<RoutePath> findAll(String departure, String arrival);
}
