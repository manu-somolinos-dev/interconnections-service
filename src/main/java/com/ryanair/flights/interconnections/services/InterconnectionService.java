package com.ryanair.flights.interconnections.services;

import com.ryanair.flights.interconnections.domain.Interconnection;

import java.time.LocalDateTime;
import java.util.List;

/**
 * services bean to manage interconnections
 */
public interface InterconnectionService {

  /**
   * find all available connections using filter parameters
   * @param departure
   * @param arrival
   * @param departureDateTime
   * @param arrivalDateTime
   * @return
   */
  List<Interconnection> findAll(String departure, String arrival, LocalDateTime departureDateTime, LocalDateTime arrivalDateTime);
}
