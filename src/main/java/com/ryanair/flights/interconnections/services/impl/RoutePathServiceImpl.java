package com.ryanair.flights.interconnections.services.impl;

import com.ryanair.flights.interconnections.entities.Route;
import com.ryanair.flights.interconnections.entities.RoutePath;
import com.ryanair.flights.interconnections.services.RoutePathService;
import com.ryanair.flights.interconnections.services.RouteService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

@Service
@Slf4j
@CacheConfig(cacheNames = { "route-paths" })
public class RoutePathServiceImpl implements RoutePathService {

  private final RouteService routeService;

  private final int maxConnectionStops;

  @Autowired
  public RoutePathServiceImpl(RouteService routeService,
                              @Value("${interconnections.constants.max-connection-stops}") int maxConnectionStops) {
    this.routeService = routeService;
    this.maxConnectionStops = maxConnectionStops;
  }

  @Override
  @Cacheable
  public List<RoutePath> findAll(String departure, String arrival) {
    log.info("FindAll valid route-paths starting at 'departure': '{}' and finished at 'arrival': '{}'", departure, arrival);
    return findValidRoutes(new LinkedList<>(), maxConnectionStops, departure, arrival);
  }

  private List<RoutePath> findValidRoutes(final LinkedList<Route> currentRoute,
                                          final int maxConnectionStops,
                                          final String departure,
                                          final String arrival) {
    if (maxConnectionStops < 0) {        // No more chances
      return asList();
    } else {                             // Not Completed RoutePath + more chances --> keep on building paths
      log.debug("Find new routes...");
      final String departureCandidate = fetchDepartureCandidate(currentRoute, departure);
      final List<String> visitedAirports = getVisitedAirports(currentRoute);

      // Filter only promising routes (from == departureCandidate, to NOT IN (all visited airports))
      final List<Route> routeCandidates = routeService.findByAirportFromAndAirportToNotIn(departureCandidate, visitedAirports);

      // Visit all candidates and explore their complete tree
      return routeCandidates.stream()
          .flatMap(route -> {
            // Exploit Candidate
            LinkedList<Route> nextCandidate = new LinkedList<>(ListUtils.union(currentRoute, asList(route)));
            LinkedList<String> nextAirportConnection = getVisitedAirports(nextCandidate);
            log.debug("Route Candidate --> [ {} ]", nextAirportConnection.stream().collect(joining(" -> ")));

            if (isCompletedRoutePath(nextCandidate, arrival)) {   // Completed RoutePath --> return result
              log.debug("Completed RoutePath --> [ {} ]", visitedAirports.stream().collect(joining(" -> ")));
              return Stream.of(new RoutePath(nextAirportConnection));
            } else {
              log.debug("Keep on exploring using Route Candidate --> [ {} ]", nextAirportConnection.stream().collect(joining(" -> ")));
              return findValidRoutes(nextCandidate, maxConnectionStops - 1, route.getAirportTo(), arrival).stream();
            }
          })
          .collect(toList());
    }
  }

  private boolean isCompletedRoutePath(LinkedList<Route> currentRoute, String arrival) {
    return !currentRoute.isEmpty() && currentRoute.getLast().getAirportTo().equals(arrival);
  }

  private String fetchDepartureCandidate(LinkedList<Route> currentRoute, String departure) {
    if (currentRoute.isEmpty()) {
      return departure;
    } else {
      return currentRoute.getLast().getAirportTo();
    }
  }

  private LinkedList<String> getVisitedAirports(LinkedList<Route> routes) {
    if (routes.isEmpty()) {
      return new LinkedList<>(Collections.emptyList());
    } else {
      Route head = routes.getFirst();
      LinkedList<String> airports = new LinkedList<>(routes.stream().map(Route::getAirportTo).collect(toList()));
      airports.addFirst(head.getAirportFrom());
      return airports;
    }
  }
}
