package com.ryanair.flights.interconnections.services;

import com.ryanair.flights.interconnections.entities.FlightSchedule;

import java.time.LocalDateTime;
import java.util.Optional;

public interface ScheduleService {
  Optional<FlightSchedule> findFlightSchedule(String departure, String arrival, LocalDateTime departureDateTime, LocalDateTime arrivalDateTime);
}
