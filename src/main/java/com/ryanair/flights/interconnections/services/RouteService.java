package com.ryanair.flights.interconnections.services;

import com.ryanair.flights.interconnections.entities.Route;

import java.util.List;

public interface RouteService {
  List<Route> findAll();
  List<Route> findByAirportFromAndAirportToNotIn(String departure, List<String> arrivals);
}
