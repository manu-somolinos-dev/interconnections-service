package com.ryanair.flights.interconnections.services.impl;

import com.ryanair.flights.interconnections.adapters.RouteAdapter;
import com.ryanair.flights.interconnections.entities.Route;
import com.ryanair.flights.interconnections.services.RouteService;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class RouteServiceImpl implements RouteService {

  private final RouteAdapter routeAdapter;
  private final List<String> acceptedOperators;

  @Autowired
  public RouteServiceImpl(RouteAdapter routeAdapter,
                          @Value("${interconnections.constants.accepted-operators}") List<String> acceptedOperators) {
    this.routeAdapter = routeAdapter;
    this.acceptedOperators = acceptedOperators;
  }

  @Override
  public List<Route> findAll() {
    return routeAdapter.findAll().parallelStream()
        .filter(this::validFlight)
        .collect(Collectors.toList());
  }

  @Override
  public List<Route> findByAirportFromAndAirportToNotIn(String departure, List<String> arrivals) {
    return findAll()
        .parallelStream()
        .filter(route -> validFlight(route)
            && route.getAirportFrom().equals(departure)
            && !arrivals.contains(route.getAirportTo()))
        .collect(Collectors.toList());
  }

  private boolean validFlight(Route route) {
    return StringUtils.isNotBlank(route.getAirportFrom())
        && StringUtils.isNotBlank(route.getAirportTo())
        && route.getConnectingAirport() == null
        && acceptedOperators.contains(route.getOperator());
  }
}
