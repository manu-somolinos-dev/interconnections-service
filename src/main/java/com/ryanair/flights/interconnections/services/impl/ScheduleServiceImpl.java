package com.ryanair.flights.interconnections.services.impl;

import com.ryanair.flights.interconnections.adapters.ScheduleAdapter;
import com.ryanair.flights.interconnections.entities.FlightSchedule;
import com.ryanair.flights.interconnections.entities.Schedule;
import com.ryanair.flights.interconnections.exception.NotFoundException;
import com.ryanair.flights.interconnections.services.ScheduleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.Optional;

import static com.ryanair.flights.interconnections.utils.DateUtils.buildLocalDateTime;
import static java.lang.String.format;
import static java.util.Optional.empty;

@Service
@Slf4j
public class ScheduleServiceImpl implements ScheduleService {

  private final ScheduleAdapter scheduleAdapter;

  @Autowired
  public ScheduleServiceImpl(ScheduleAdapter scheduleAdapter) {
    this.scheduleAdapter = scheduleAdapter;
  }

  @Override
  public Optional<FlightSchedule> findFlightSchedule(String departure,
                                                     String arrival,
                                                     LocalDateTime departureDateTime,
                                                     LocalDateTime arrivalDateTime) {
    int year = departureDateTime.getYear();
    int month = departureDateTime.getMonth().getValue();
    try {
      Schedule schedule = scheduleAdapter.find(departure, arrival, year, month);
      return fetchEagerFlightSchedule(schedule, departureDateTime, arrivalDateTime);
    } catch (NotFoundException e) {
      log.debug("No Flight Schedule available for [{} -> {}] on Year {}/Month {}", departure, arrival, year, month);
      return empty();
    }
  }

  /**
   * Given a schedule, find first flight schedule valid
   * @param schedule
   * @param departureDateTime
   * @param arrivalDateTime
   * @return
   */
  private Optional<FlightSchedule> fetchEagerFlightSchedule(Schedule schedule,
                                                            LocalDateTime departureDateTime,
                                                            LocalDateTime arrivalDateTime) {
      return schedule.getDays().stream()
          .filter(daySchedule -> daySchedule.getDay() == departureDateTime.getDayOfMonth())
          .flatMap(daySchedule -> daySchedule.getFlights().stream())
          .sorted(Comparator.comparing(FlightSchedule::getDepartureTime))
          .filter(flightSchedule -> isValidFlightSchedule(flightSchedule, departureDateTime, arrivalDateTime))
          .findFirst();
  }

  /**
   * verify flight-schedule occurs in a compatible time-schedule for flight interconnection:
   *  - departure-time <= flight-schedule.departure-time
   *  - arrival-time > flight-schedule.arrival-time
   * @param flightSchedule
   * @param departureDateTime
   * @param arrivalDateTime
   * @return
   */
  private boolean isValidFlightSchedule(FlightSchedule flightSchedule, LocalDateTime departureDateTime, LocalDateTime arrivalDateTime) {
    LocalDateTime flightDepartureDateTime = buildLocalDateTime(departureDateTime, flightSchedule.getDepartureTime());
    LocalDateTime flightArrivalDateTime = buildLocalDateTime(departureDateTime, flightSchedule.getArrivalTime());
    if (flightSchedule.getArrivalTime().isBefore(flightSchedule.getDepartureTime())) {
      flightArrivalDateTime = flightDepartureDateTime.plusDays(1);
    }
    return flightDepartureDateTime.isAfter(departureDateTime) && arrivalDateTime.isAfter(flightArrivalDateTime);
  }
}
