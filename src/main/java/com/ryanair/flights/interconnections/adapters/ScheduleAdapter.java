package com.ryanair.flights.interconnections.adapters;

import com.ryanair.flights.interconnections.entities.Schedule;

public interface ScheduleAdapter extends BaseAdapter {
  Schedule find(String origin, String destination, int year, int month);
}
