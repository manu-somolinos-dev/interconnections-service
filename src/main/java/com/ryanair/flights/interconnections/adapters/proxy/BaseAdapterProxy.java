package com.ryanair.flights.interconnections.adapters.proxy;

import com.ryanair.flights.interconnections.exception.HealthException;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import static java.lang.String.format;

@Slf4j
public abstract class BaseAdapterProxy {

  protected final RestTemplate restTemplate;
  protected final String baseUrl;
  protected final String requestPath;
  protected final String health;

  protected BaseAdapterProxy(RestTemplate restTemplate, String baseUrl, String requestPath, String health) {
    this.restTemplate = restTemplate;
    this.baseUrl = baseUrl;
    this.requestPath = requestPath;
    this.health = health;
  }

  protected abstract Logger getLog();

  public void health() {
    try {
      ResponseEntity response = restTemplate.getForEntity(buildUrlRequest(health), Void.class);
      if (!response.getStatusCode().is2xxSuccessful()) {
        String message = format("Invalid Health Status: %d", response.getStatusCodeValue());
        getLog().error(message);
        throw new HealthException(message);
      }
    } catch (RuntimeException e) {
      String message = format("Invalid Health Status Exception: %s", e.getMessage());
      getLog().error(message, e.getCause());
      throw new HealthException(message);
    }
  }

  protected String buildUrlRequest(String... pathSegment) {
    return UriComponentsBuilder.fromHttpUrl(baseUrl).pathSegment(pathSegment).build().toUriString();
  }
}
