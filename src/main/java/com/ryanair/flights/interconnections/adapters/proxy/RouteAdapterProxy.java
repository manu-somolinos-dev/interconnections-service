package com.ryanair.flights.interconnections.adapters.proxy;

import com.ryanair.flights.interconnections.adapters.RouteAdapter;
import com.ryanair.flights.interconnections.entities.Route;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;

@Service
@Slf4j
@CacheConfig(cacheNames = { "routes" })
public class RouteAdapterProxy extends BaseAdapterProxy implements RouteAdapter {

  @Autowired
  public RouteAdapterProxy(RestTemplate restTemplate,
                           @Value("${interconnections.proxy.adapter.routes.base-url}") String baseUrl,
                           @Value("${interconnections.proxy.adapter.routes.request-path}") String requestPath,
                           @Value("${interconnections.proxy.adapter.routes.health}") String health) {
    super(restTemplate, baseUrl, requestPath, health);
  }

  @Override
  protected Logger getLog() {
    return log;
  }

  @Override
  @Cacheable
  public List<Route> findAll() {
    getLog().info("find all available routes");
    try {
      return asList(restTemplate.getForObject(buildUrlRequest(requestPath), Route[].class));
    } catch (RuntimeException e) {
      getLog().error("Unable to fetch remote routes[{}]: {}", buildUrlRequest(requestPath), e.getMessage(), e);
      return Collections.emptyList();
    }
  }

}
