package com.ryanair.flights.interconnections.adapters;

import com.ryanair.flights.interconnections.entities.Route;

import java.util.List;

public interface RouteAdapter extends BaseAdapter {
  List<Route> findAll();
}
