package com.ryanair.flights.interconnections.adapters.proxy;

import com.ryanair.flights.interconnections.adapters.ScheduleAdapter;
import com.ryanair.flights.interconnections.entities.Schedule;
import com.ryanair.flights.interconnections.exception.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import static java.lang.String.format;
import static java.lang.String.valueOf;

@Service
@Slf4j
public class ScheduleAdapterProxy extends BaseAdapterProxy implements ScheduleAdapter {

  @Autowired
  public ScheduleAdapterProxy(RestTemplate restTemplate,
                              @Value("${interconnections.proxy.adapter.schedules.base-url}") String baseUrl,
                              @Value("${interconnections.proxy.adapter.schedules.request-path}") String requestPath,
                              @Value("${interconnections.proxy.adapter.schedules.health}") String health) {
    super(restTemplate, baseUrl, requestPath, health);
  }

  @Override
  protected Logger getLog() {
    return log;
  }

  @Override
  public Schedule find(String departure, String arrival, int year, int month) {
    getLog().debug("find a suitable schedule: ['departure': '{}', 'arrival': '{}', 'year': '{}', 'month': '{}']",
        departure, arrival, year, month);
    String url = buildUrlRequest(requestPath, departure, arrival, "years", valueOf(year), "months", valueOf(month));
    try {
      return restTemplate.getForObject(url, Schedule.class);
    } catch (RuntimeException e) {
      String message = format("Flight Schedule [%s -> %s] unavailable at year %d and month %d",
          departure, arrival, year, month);
      log.debug(message);
      throw new NotFoundException(message);
    }
  }
}
