package com.ryanair.flights.interconnections.adapters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ryanair.flights.interconnections.adapters.proxy.RouteAdapterProxy;
import com.ryanair.flights.interconnections.entities.Route;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
@DisplayName("route adapter test case")
public class RouteAdapterTest {

  @MockBean
  private RestTemplate restTemplate;

  private ObjectMapper objectMapper = new Jackson2ObjectMapperBuilder().build();

  private RouteAdapter routeAdapter;

  @BeforeEach
  public void setUp() {
    routeAdapter = new RouteAdapterProxy(restTemplate, "https://services-api.ryanair.com/locate", "3/routes", "health");
  }

  @Test
  public void findAll() throws IOException {
    // Mocks
    given(restTemplate.getForObject(anyString(), eq(Route[].class)))
        .willReturn(foundData());

    // Given-When
    List<Route> actual = routeAdapter.findAll();

    // Then
    assertAll("rest-template-find-ok",
        () -> assertThat(actual.size()).isEqualTo(13));

    // And
    verify(restTemplate, times(1))
        .getForObject(eq("https://services-api.ryanair.com/locate/3/routes"), eq(Route[].class));
  }

  private Route[] foundData() throws IOException {
    String rawData = "[{\"airportFrom\":\"AAL\",\"airportTo\":\"STN\",\"connectingAirport\":null,\"newRoute\":false,\"seasonalRoute\":false,\"operator\":\"RYANAIR\",\"group\":\"CITY\",\"similarArrivalAirportCodes\":[],\"tags\":[],\"carrierCode\":\"FR\"}," +
        "{\"airportFrom\":\"AAR\",\"airportTo\":\"DUB\",\"connectingAirport\":null,\"newRoute\":false,\"seasonalRoute\":false,\"operator\":\"RYANAIR\",\"group\":\"GENERIC\",\"similarArrivalAirportCodes\":[],\"tags\":[],\"carrierCode\":\"FR\"}," +
        "{\"airportFrom\":\"AAR\",\"airportTo\":\"GDN\",\"connectingAirport\":null,\"newRoute\":false,\"seasonalRoute\":false,\"operator\":\"RYANAIR\",\"group\":\"ETHNIC\",\"similarArrivalAirportCodes\":[],\"tags\":[],\"carrierCode\":\"FR\"}," +
        "{\"airportFrom\":\"AAR\",\"airportTo\":\"STN\",\"connectingAirport\":null,\"newRoute\":false,\"seasonalRoute\":false,\"operator\":\"RYANAIR\",\"group\":\"CITY\",\"similarArrivalAirportCodes\":[],\"tags\":[],\"carrierCode\":\"FR\"}," +
        "{\"airportFrom\":\"ABZ\",\"airportTo\":\"AGP\",\"connectingAirport\":null,\"newRoute\":false,\"seasonalRoute\":false,\"operator\":\"RYANAIR\",\"group\":\"LEISURE\",\"similarArrivalAirportCodes\":[],\"tags\":[],\"carrierCode\":\"FR\"}," +
        "{\"airportFrom\":\"ABZ\",\"airportTo\":\"ALC\",\"connectingAirport\":null,\"newRoute\":false,\"seasonalRoute\":false,\"operator\":\"RYANAIR\",\"group\":\"LEISURE\",\"similarArrivalAirportCodes\":[],\"tags\":[],\"carrierCode\":\"FR\"}," +
        "{\"airportFrom\":\"ABZ\",\"airportTo\":\"FAO\",\"connectingAirport\":null,\"newRoute\":false,\"seasonalRoute\":false,\"operator\":\"RYANAIR\",\"group\":\"LEISURE\",\"similarArrivalAirportCodes\":[],\"tags\":[],\"carrierCode\":\"FR\"}," +
        "{\"airportFrom\":\"ABZ\",\"airportTo\":\"MLA\",\"connectingAirport\":null,\"newRoute\":false,\"seasonalRoute\":false,\"operator\":\"RYANAIR\",\"group\":\"LEISURE\",\"similarArrivalAirportCodes\":[],\"tags\":[],\"carrierCode\":\"FR\"}," +
        "{\"airportFrom\":\"ACE\",\"airportTo\":\"ATH\",\"connectingAirport\":\"BGY\",\"newRoute\":false,\"seasonalRoute\":false,\"operator\":\"RYANAIR\",\"group\":\"GENERIC\",\"similarArrivalAirportCodes\":[],\"tags\":[],\"carrierCode\":\"FR\"}," +
        "{\"airportFrom\":\"ACE\",\"airportTo\":\"BDS\",\"connectingAirport\":\"BGY\",\"newRoute\":false,\"seasonalRoute\":false,\"operator\":\"RYANAIR\",\"group\":\"GENERIC\",\"similarArrivalAirportCodes\":[],\"tags\":[],\"carrierCode\":\"FR\"}," +
        "{\"airportFrom\":\"ACE\",\"airportTo\":\"BFS\",\"connectingAirport\":null,\"newRoute\":false,\"seasonalRoute\":false,\"operator\":\"RYANAIR\",\"group\":\"CANARY\",\"similarArrivalAirportCodes\":[],\"tags\":[],\"carrierCode\":\"FR\"}," +
        "{\"airportFrom\":\"ACE\",\"airportTo\":\"BGY\",\"connectingAirport\":null,\"newRoute\":false,\"seasonalRoute\":false,\"operator\":\"RYANAIR\",\"group\":\"CANARY\",\"similarArrivalAirportCodes\":[\"BHX\",\"BLQ\",\"CRL\",\"DUB\",\"MAD\",\"STN\",\"SVQ\"],\"tags\":[],\"carrierCode\":\"FR\"}," +
        "{\"airportFrom\":\"ACE\",\"airportTo\":\"BHX\",\"connectingAirport\":null,\"newRoute\":false,\"seasonalRoute\":false,\"operator\":\"RYANAIR\",\"group\":\"CANARY\",\"similarArrivalAirportCodes\":[\"BGY\",\"EDI\",\"LPL\",\"MAD\",\"PIK\",\"STN\",\"SVQ\"],\"tags\":[],\"carrierCode\":\"FR\"}]";
    return objectMapper.readValue(rawData, Route[].class);
  }
}