package com.ryanair.flights.interconnections.adapters;

import com.ryanair.flights.interconnections.entities.Route;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.util.List;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DisplayName("remote request to ryanair api test case")
public class RyanairRemoteTest {

  @Test
  @DisplayName("request to get routes endpoint")
  public void test_get_routes() {
    // Given
    String url = "https://services-api.ryanair.com/locate/3/routes";
    RestTemplate restTemplate = new RestTemplate();
    // When
    List<Route> results = asList(restTemplate.getForObject(url, Route[].class));
    // Then
    assertThat(results).isNotNull();
  }

  @Test
  @DisplayName("request to get routes and expires read-connection")
  public void test_get_routes_expires_read_connection()  {
    // Given
    String url = "https://services-api.ryanair.com/locate/3/routes";
    RestTemplate restTemplate = new RestTemplateBuilder()
        .setReadTimeout(Duration.ofSeconds(1))
        .setConnectTimeout(Duration.ofSeconds(1))
        .build();
    // When
    List<Route> results = asList(restTemplate.getForObject(url, Route[].class));
    // Then
    assertThat(results).isNotNull();
  }
}
