package com.ryanair.flights.interconnections.adapters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ryanair.flights.interconnections.adapters.proxy.ScheduleAdapterProxy;
import com.ryanair.flights.interconnections.entities.Schedule;
import com.ryanair.flights.interconnections.exception.NotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.time.LocalTime;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@DisplayName("scheduler adapter test case")
@ExtendWith(SpringExtension.class)
public class ScheduleAdapterTest {

  @MockBean
  private RestTemplate restTemplate;

  private ObjectMapper objectMapper = new Jackson2ObjectMapperBuilder().build();

  private ScheduleAdapter scheduleAdapter;

  @BeforeEach
  public void setUp() {
    scheduleAdapter = new ScheduleAdapterProxy(restTemplate, "https://services-api.ryanair.com/timtbl", "3/schedules", "health");
  }

  @Test
  public void find_and_found() throws IOException {
    // Mocks
    given(restTemplate.getForObject(anyString(), eq(Schedule.class)))
        .willReturn(foundData());

    // When
    Schedule actual = scheduleAdapter.find("MAD", "IBZ", 2019, 10);

    // Then
    assertAll("rest-template-request-ok",
        () -> assertThat(actual.getMonth()).isEqualTo(10),
        () -> assertThat(actual.getDays().size()).isEqualTo(31),
        () -> assertThat(actual.getDays().get(0).getFlights()).isNotEmpty(),
        () -> assertThat(actual.getDays().get(0).getFlights().get(0).getDepartureTime()).isEqualTo(LocalTime.of(6, 40)),
        () -> assertThat(actual.getDays().get(0).getFlights().get(0).getArrivalTime()).isEqualTo(LocalTime.of(8, 0))
    );

    // And
    verify(restTemplate, times(1))
        .getForObject(eq("https://services-api.ryanair.com/timtbl/3/schedules/MAD/IBZ/years/2019/months/10"), eq(Schedule.class));
  }

  private Schedule foundData() throws IOException {
    String raw = "{\"month\":10,\"days\":[" +
        "{\"day\":1,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"06:40\",\"arrivalTime\":\"08:00\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:15\",\"arrivalTime\":\"21:35\"}]}," +
        "{\"day\":2,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"06:40\",\"arrivalTime\":\"08:00\"},{\"carrierCode\":\"FR\",\"number\":\"2066\",\"departureTime\":\"12:50\",\"arrivalTime\":\"14:10\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:15\",\"arrivalTime\":\"21:35\"}]}," +
        "{\"day\":3,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"06:40\",\"arrivalTime\":\"08:00\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:15\",\"arrivalTime\":\"21:35\"}]}," +
        "{\"day\":4,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"06:40\",\"arrivalTime\":\"08:00\"},{\"carrierCode\":\"FR\",\"number\":\"2066\",\"departureTime\":\"12:50\",\"arrivalTime\":\"14:10\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:15\",\"arrivalTime\":\"21:35\"}]}," +
        "{\"day\":5,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"06:40\",\"arrivalTime\":\"08:00\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:15\",\"arrivalTime\":\"21:35\"}]}," +
        "{\"day\":6,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"06:40\",\"arrivalTime\":\"08:00\"},{\"carrierCode\":\"FR\",\"number\":\"2066\",\"departureTime\":\"12:50\",\"arrivalTime\":\"14:10\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:15\",\"arrivalTime\":\"21:35\"}]}," +
        "{\"day\":7,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"06:40\",\"arrivalTime\":\"08:00\"},{\"carrierCode\":\"FR\",\"number\":\"2066\",\"departureTime\":\"12:50\",\"arrivalTime\":\"14:10\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:15\",\"arrivalTime\":\"21:35\"}]}," +
        "{\"day\":8,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"06:40\",\"arrivalTime\":\"08:00\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:15\",\"arrivalTime\":\"21:35\"}]}," +
        "{\"day\":9,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"06:40\",\"arrivalTime\":\"08:00\"},{\"carrierCode\":\"FR\",\"number\":\"2066\",\"departureTime\":\"12:50\",\"arrivalTime\":\"14:10\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:15\",\"arrivalTime\":\"21:35\"}]}," +
        "{\"day\":10,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"06:40\",\"arrivalTime\":\"08:00\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:15\",\"arrivalTime\":\"21:35\"}]}," +
        "{\"day\":11,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"06:40\",\"arrivalTime\":\"08:00\"},{\"carrierCode\":\"FR\",\"number\":\"2066\",\"departureTime\":\"12:50\",\"arrivalTime\":\"14:10\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:15\",\"arrivalTime\":\"21:35\"}]}," +
        "{\"day\":12,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"06:40\",\"arrivalTime\":\"08:00\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:15\",\"arrivalTime\":\"21:35\"}]}," +
        "{\"day\":13,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"06:40\",\"arrivalTime\":\"08:00\"},{\"carrierCode\":\"FR\",\"number\":\"2066\",\"departureTime\":\"12:50\",\"arrivalTime\":\"14:10\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:15\",\"arrivalTime\":\"21:35\"}]}," +
        "{\"day\":14,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"06:40\",\"arrivalTime\":\"08:00\"},{\"carrierCode\":\"FR\",\"number\":\"2066\",\"departureTime\":\"12:50\",\"arrivalTime\":\"14:10\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:15\",\"arrivalTime\":\"21:35\"}]}," +
        "{\"day\":15,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"06:40\",\"arrivalTime\":\"08:00\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:15\",\"arrivalTime\":\"21:35\"}]}," +
        "{\"day\":16,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"06:40\",\"arrivalTime\":\"08:00\"},{\"carrierCode\":\"FR\",\"number\":\"2066\",\"departureTime\":\"12:50\",\"arrivalTime\":\"14:10\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:15\",\"arrivalTime\":\"21:35\"}]}," +
        "{\"day\":17,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"06:40\",\"arrivalTime\":\"08:00\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:15\",\"arrivalTime\":\"21:35\"}]}," +
        "{\"day\":18,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"06:40\",\"arrivalTime\":\"08:00\"},{\"carrierCode\":\"FR\",\"number\":\"2066\",\"departureTime\":\"12:50\",\"arrivalTime\":\"14:10\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:15\",\"arrivalTime\":\"21:35\"}]}," +
        "{\"day\":19,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"06:40\",\"arrivalTime\":\"08:00\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:15\",\"arrivalTime\":\"21:35\"}]}," +
        "{\"day\":20,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"06:40\",\"arrivalTime\":\"08:00\"},{\"carrierCode\":\"FR\",\"number\":\"2066\",\"departureTime\":\"12:50\",\"arrivalTime\":\"14:10\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:15\",\"arrivalTime\":\"21:35\"}]}," +
        "{\"day\":21,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"06:40\",\"arrivalTime\":\"08:00\"},{\"carrierCode\":\"FR\",\"number\":\"2066\",\"departureTime\":\"12:50\",\"arrivalTime\":\"14:10\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:15\",\"arrivalTime\":\"21:35\"}]}," +
        "{\"day\":22,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"06:40\",\"arrivalTime\":\"08:00\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:15\",\"arrivalTime\":\"21:35\"}]}," +
        "{\"day\":23,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"06:40\",\"arrivalTime\":\"08:00\"},{\"carrierCode\":\"FR\",\"number\":\"2066\",\"departureTime\":\"12:50\",\"arrivalTime\":\"14:10\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:15\",\"arrivalTime\":\"21:35\"}]}," +
        "{\"day\":24,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"06:40\",\"arrivalTime\":\"08:00\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:15\",\"arrivalTime\":\"21:35\"}]}," +
        "{\"day\":25,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"06:40\",\"arrivalTime\":\"08:00\"},{\"carrierCode\":\"FR\",\"number\":\"2066\",\"departureTime\":\"12:50\",\"arrivalTime\":\"14:10\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:15\",\"arrivalTime\":\"21:35\"}]}," +
        "{\"day\":26,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"06:40\",\"arrivalTime\":\"08:00\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:15\",\"arrivalTime\":\"21:35\"}]}," +
        "{\"day\":27,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"07:10\",\"arrivalTime\":\"08:30\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:00\",\"arrivalTime\":\"21:20\"}]}," +
        "{\"day\":28,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"07:40\",\"arrivalTime\":\"09:00\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:00\",\"arrivalTime\":\"21:20\"}]}," +
        "{\"day\":29,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"07:10\",\"arrivalTime\":\"08:30\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"18:55\",\"arrivalTime\":\"20:15\"}]}," +
        "{\"day\":30,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"07:10\",\"arrivalTime\":\"08:30\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:00\",\"arrivalTime\":\"21:20\"}]}," +
        "{\"day\":31,\"flights\":[{\"carrierCode\":\"FR\",\"number\":\"5354\",\"departureTime\":\"07:10\",\"arrivalTime\":\"08:30\"},{\"carrierCode\":\"FR\",\"number\":\"2084\",\"departureTime\":\"20:00\",\"arrivalTime\":\"21:20\"}]}]}";
    return objectMapper.readValue(raw, Schedule.class);
  }

  @Test
  public void find_and_not_found() {
    // Mocks
    given(restTemplate.getForObject(anyString(), eq(Schedule.class)))
        .willThrow(new HttpClientErrorException(
              NOT_FOUND,
              NOT_FOUND.getReasonPhrase(),
              "{\"code\":\"ResourceNotFound\",\"message\":\"Resource not found\"}".getBytes(),
              UTF_8
            )
        );

    // When
    NotFoundException notFoundException = assertThrows(NotFoundException.class,
        () -> scheduleAdapter.find("MAD", "IBZ", 2019, 3)
    );

    assertAll("not-found-exception",
        () -> assertThat(notFoundException.getMessage()).isEqualTo("Flight Schedule [MAD -> IBZ] unavailable at year 2019 and month 3")
    );

    // And
    verify(restTemplate, times(1))
        .getForObject(eq("https://services-api.ryanair.com/timtbl/3/schedules/MAD/IBZ/years/2019/months/3"), eq(Schedule.class));
  }
}