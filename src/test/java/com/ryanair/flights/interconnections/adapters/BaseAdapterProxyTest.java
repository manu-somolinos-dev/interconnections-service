package com.ryanair.flights.interconnections.adapters;

import com.ryanair.flights.interconnections.adapters.proxy.BaseAdapterProxy;
import com.ryanair.flights.interconnections.exception.HealthException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.http.HttpStatus.FOUND;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@ExtendWith(SpringExtension.class)
@DisplayName("base adapter test case")
public class BaseAdapterProxyTest {

  @MockBean
  private Logger log;

  @MockBean
  private RestTemplate restTemplate;

  private BaseAdapterProxy baseAdapterProxy;

  @BeforeEach
  public void setUp() {
    baseAdapterProxy = new BaseAdapterProxy(restTemplate, "http://service:8080", "request-path", "health") {
      @Override
      protected Logger getLog() {
        return log;
      }
    };
  }

  @Test
  @DisplayName("given a health endpoint ok when a health request is request then nothing happens")
  public void health_up() {
    // Mocks
    given(restTemplate.getForEntity(anyString(), eq(Void.class)))
        .willReturn(ResponseEntity.ok().build());

    // Given-When
    baseAdapterProxy.health();

    // Then
    verify(restTemplate, times(1)).getForEntity(eq("http://service:8080/health"), eq(Void.class));
  }

  @Test
  @DisplayName("given a health endpoint which is not healthy when a health request is request then a health exception is thrown")
  public void health_down_not_ok() {
    // Mocks
    given(restTemplate.getForEntity(anyString(), eq(Void.class)))
        .willReturn(ResponseEntity.status(FOUND).build());

    // Given-When-Then
    HealthException actualException = assertThrows(HealthException.class,
        () -> baseAdapterProxy.health()
    );
    assertAll("Health not UP",
        () -> actualException.getMessage().equals("Invalid Health Status: 302"));

    // And
    verify(restTemplate, times(1)).getForEntity(eq("http://service:8080/health"), eq(Void.class));
  }

  @Test
  @DisplayName("given a health endpoint which throws exception when a health request is request then a health exception is thrown")
  public void health_down_handle_exception() {
    // Mocks
    given(restTemplate.getForEntity(anyString(), eq(Void.class)))
        .willThrow(new HttpClientErrorException(INTERNAL_SERVER_ERROR));

    // Given-When-Then
    HealthException actualException = assertThrows(HealthException.class,
        () -> baseAdapterProxy.health()
    );
    assertAll("Health is DOWN",
        () -> actualException.getMessage().equals("Invalid Health Status Exception: " + INTERNAL_SERVER_ERROR.getReasonPhrase()));

    // And
    verify(restTemplate, times(1)).getForEntity(eq("http://service:8080/health"), eq(Void.class));
  }
}