package com.ryanair.flights.interconnections;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

@DisplayName("interconnections component context test case")
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class InterconnectionServiceApplicationTests {

	@Autowired
	private ApplicationContext context;

	@Test
	@DisplayName("given current configuration when context startup is triggered it succeed")
	public void contextLoads() {
		assertAll("context", () -> assertThat(context).isNotNull());
	}

}
