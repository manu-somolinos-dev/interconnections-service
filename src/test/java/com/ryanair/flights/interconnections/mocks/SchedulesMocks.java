package com.ryanair.flights.interconnections.mocks;

import com.ryanair.flights.interconnections.entities.DaySchedule;
import com.ryanair.flights.interconnections.entities.FlightSchedule;
import com.ryanair.flights.interconnections.entities.Schedule;

import java.time.LocalTime;

import static java.util.Arrays.asList;

public class SchedulesMocks {

  public static Schedule schedule(String departure, String arrival, int month, DaySchedule... daySchedules) {
    return new Schedule(month, asList(daySchedules));
  }

  public static FlightSchedule flightSchedule(LocalTime departure, LocalTime arrival) {
    return new FlightSchedule("FR", "1926", departure, arrival);
  }

  public static DaySchedule daySchedule(int dayOfMonth, FlightSchedule... flightSchedules) {
    return new DaySchedule(dayOfMonth, asList(flightSchedules));
  }
}
