package com.ryanair.flights.interconnections.mocks;

import com.ryanair.flights.interconnections.entities.RoutePath;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

public class RoutePathMocks {

  public static List<RoutePath> emptyRoutePaths() {
    return emptyList();
  }

  public static List<RoutePath> emptyConnectionsRoutePath() {
    return asList(routePath("MAD", "BCN", asList("MAD", "BCN")));
  }

  public static List<RoutePath> oneConnectionsMaxRoutePath() {
    return asList(
        routePath("MAD", "BCN", asList("MAD", "BCN")),
        routePath("MAD", "BCN", asList("MAD", "IBZ", "BCN")),
        routePath("MAD", "BCN", asList("MAD", "LGW", "BCN"))
    );
  }

  public static List<RoutePath> twoConnectionsMaxRoutePath() {
    return asList(
        routePath("MAD", "BCN", asList("MAD", "BCN")),
        routePath("MAD", "BCN", asList("MAD", "IBZ", "BCN")),
        routePath("MAD", "BCN", asList("MAD", "IBZ", "DUB", "BCN")),
        routePath("MAD", "BCN", asList("MAD", "DUB", "BCN")),
        routePath("MAD", "BCN", asList("MAD", "DUB", "IBZ", "BCN")),
        routePath("MAD", "BCN", asList("MAD", "LGW", "BCN")),
        routePath("MAD", "BCN", asList("MAD", "LGW", "GTW", "BCN")),
        routePath("MAD", "BCN", asList("MAD", "GTW", "BCN")),
        routePath("MAD", "BCN", asList("MAD", "GTW", "LGW", "BCN")),
        routePath("MAD", "BCN", asList("MAD", "ORY", "BCN")),
        routePath("MAD", "BCN", asList("MAD", "ORY", "CDG", "BCN")),
        routePath("MAD", "BCN", asList("MAD", "CDG", "ORY", "BCN")),
        routePath("MAD", "BCN", asList("MAD", "CDG", "BCN")),
        routePath("MAD", "BCN", asList("MAD", "FCO", "BCN")),
        routePath("MAD", "BCN", asList("MAD", "FCO", "SXF", "BCN")),
        routePath("MAD", "BCN", asList("MAD", "SXF", "BCN")),
        routePath("MAD", "BCN", asList("MAD", "SXF", "FCO", "BCN"))
        );
  }

  private static RoutePath routePath(String departure, String arrival, List<String> connections) {
    return new RoutePath(departure, arrival, connections);
  }
}
