package com.ryanair.flights.interconnections.mocks;

import com.ryanair.flights.interconnections.entities.Route;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

public class RoutesMock {

  private static List<String> airports = asList("MAD", "BCN", "IBZ", "LTN", "LGW", "LHR", "DUB", "ORY", "CDG", "FRA", "MUC", "SXF", "FCO", "CIA", "BRU", "AMS");

  public static List<Route> emptyRoutes() {
    return emptyList();
  }

  public static List<Route> noStopRoutes(String departure, String arrival) {
    return asList(route(departure, arrival));
  }

  public static List<Route> oneStopRoutes(String departure, String arrival) {
    return asList(
        route(departure, "LGW"),
        route("LGW", arrival),
        route(departure, "LTN"),
        route("LTN", arrival),
        route(departure, "DUB"),
        route("DUB", arrival),
        route(departure, "IBZ"),
        route("IBZ", arrival),
        route(departure, "SXF"),
        route("SXF", arrival),
        route(departure, "ORY"),
        route("ORY", arrival),
        route(departure, "CDG"),
        route("CDG", arrival)
    );
  }

  public static List<Route> twoStopRoutes(String departure, String arrival) {
    return asList(
        route(departure, "LGW"),
        route("LGW", arrival),
        route("LGW", "LTN"),
        route("LTN", "LGW"),
        route(departure, "LTN"),
        route("LTN", arrival),
        route(departure, "DUB"),
        route("DUB", arrival),
        route(departure, "IBZ"),
        route("IBZ", arrival),
        route("DUB", "IBZ"),
        route("IBZ", "DUB"),
        route(departure, "SXF"),
        route("SXF", arrival),
        route(departure, "ORY"),
        route("ORY", arrival),
        route("SXF", "ORY"),
        route("ORY", "SXF"),
        route(departure, "CDG"),
        route("CDG", arrival),
        route(departure, "FCI"),
        route("FCI", arrival),
        route("CDG", "FCI"),
        route("FCI", "CDG")
    );
  }

  public static Route route(String departure, String arrival) {
    return new Route(departure, arrival, null, false, false, "RYANAIR", "CITY", emptyList(), emptyList(), "FR");
  }
}
