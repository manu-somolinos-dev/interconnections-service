package com.ryanair.flights.interconnections.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ryanair.flights.interconnections.domain.Interconnection;
import com.ryanair.flights.interconnections.domain.Leg;
import com.ryanair.flights.interconnections.services.InterconnectionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDateTime;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("interconnections controller test case")
@ExtendWith(SpringExtension.class)
@WebMvcTest(InterconnectionsController.class)
public class InterconnectionsControllerTest {

  private MockMvc mockMvc;

  @MockBean
  private InterconnectionService interconnectionService;

  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  private WebApplicationContext context;

  private LocalDateTime now = LocalDateTime.now();

  @BeforeEach
  public void setUp() {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
        .build();

    // mocks
    given(interconnectionService.findAll(anyString(), anyString(), any(LocalDateTime.class), any(LocalDateTime.class)))
        .willReturn(asList(new Interconnection(0, asList(new Leg("MAD", "BCN", now, now.plusDays(1))))));
  }

  @Test
  @DisplayName("given the url interconnections endpoint when a request is triggered it succeed")
  public void test_when_get_interconnections_then_returns_ok() throws Exception {
    // Mocks
    List<Interconnection> expected = asList(new Interconnection(0, asList(new Leg("MAD", "BCN", now, now.plusDays(1)))));

    // Given
    String url = "/interconnections";

    // When
    this.mockMvc.perform(get(url)
        .param("departure", "MAD")
        .param("arrival", "BCN")
        .param("departureDateTime", "2019-11-07T08:00")
        .param("arrivalDateTime", "2019-11-18T20:00")
    )
        // Then
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        .andExpect(content().json(objectMapper.writeValueAsString(expected)));
  }

  @Test
  @DisplayName("given the url interconnections endpoint but not results when a request is triggered not found exception thrown")
  public void test_when_get_interconnections_valid_input_empty_results_then_not_found_exception() throws Exception {

    // Mocks
    given(interconnectionService.findAll(anyString(), anyString(), any(LocalDateTime.class), any(LocalDateTime.class)))
        .willReturn(asList());

    // Given
    String url = "/interconnections";

    // When
    this.mockMvc.perform(get(url)
        .param("departure", "MAD")
        .param("arrival", "BCN")
        .param("departureDateTime", "2019-11-07T08:00")
        .param("arrivalDateTime", "2019-11-18T20:00")
    )
        // Then
        .andExpect(status().isNotFound())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
  }

  @Test
  @DisplayName("given the url interconnections endpoint and insufficient params when a request is triggered it fails returning bad request exception")
  public void test_when_get_interconnections_with_insufficient_params_then_returns_bad_request_exception() throws Exception {
    // Mocks
    List<Interconnection> expected = emptyList();

    // Given
    String url = "/interconnections";

    // When
    this.mockMvc.perform(get(url)
        .param("departure", "MAD")
        .param("arrival", "BCN")
    )
        // Then
        .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("given the url interconnections endpoint and bad params when a request is triggered it fails returning bad request exception")
  public void test_when_get_interconnections_with_bad_params_then_returns_bad_request_exception() throws Exception {
    // Mocks
    List<Interconnection> expected = emptyList();

    // Given
    String url = "/interconnections";

    // When
    this.mockMvc.perform(get(url)
        .param("departure", "")
        .param("arrival", "BCN")
        .param("departureDateTime", "2019-11-07T08:00")
        .param("arrivalDateTime", "2019-11-18T20:00")
    )
        // Then
        .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("given the url interconnections endpoint and invalid params when a request is triggered it fails returning bad request exception")
  public void test_when_get_interconnections_with_invalid_params_then_returns_bad_request_exception() throws Exception {
    // Mocks
    List<Interconnection> expected = emptyList();

    // Given
    String url = "/interconnections";

    // When
    this.mockMvc.perform(get(url)
        .param("departure", "MAD")
        .param("arrival", "BCN")
        .param("departureDateTime", "true")
        .param("arrivalDateTime", "2019-11-18T20:00")
    )
        // Then
        .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("given the url interconnections endpoint when a request results in internal server error then it fails")
  public void test_when_get_interconnections_and_unexpected_error_then_returns_error_500() throws Exception {
    // Mocks
    doThrow(new RuntimeException("Danger!!"))
        .when(interconnectionService).findAll(anyString(), anyString(), any(LocalDateTime.class), any(LocalDateTime.class));

    // Given
    String url = "/interconnections";

    // When
    this.mockMvc.perform(get(url)
        .param("departure", "MAD")
        .param("arrival", "BCN")
        .param("departureDateTime", "2019-11-07T08:00")
        .param("arrivalDateTime", "2019-11-18T20:00")
    )
        // Then
        .andExpect(status().isInternalServerError())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
  }

  @Test
  public void test_when_request_not_found_path_then_returns_error() throws Exception {
    // Given
    String url = "/other-path";

    // When
    this.mockMvc.perform(get(url))
        // Then
        .andExpect(status().isNotFound());
  }
}
