package com.ryanair.flights.interconnections.services;

import com.ryanair.flights.interconnections.entities.RoutePath;
import com.ryanair.flights.interconnections.mocks.RoutesMock;
import com.ryanair.flights.interconnections.services.impl.RoutePathServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashSet;
import java.util.List;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.BDDMockito.given;

@ExtendWith(SpringExtension.class)
@DisplayName("route path service test case")
public class RoutePathServiceTest {

  @MockBean
  private RouteService routeService;

  private RoutePathServiceImpl routePathService;

  @BeforeEach
  public void setUp() {
    routePathService = new RoutePathServiceImpl(routeService, 2);
  }

  @Test
  @DisplayName("given some mocked routes, departure and arrival input criteria filter when find all then return some results")
  public void test_find_all_with_departure_arrival_criteria() {
    // Mocks
    given(routeService.findAll())
        .willReturn(asList(
            RoutesMock.route("MAD", "BCN"),
            RoutesMock.route("MAD", "IBZ"),
            RoutesMock.route("MAD", "DUB"),
            RoutesMock.route("MAD", "ORY"),
            RoutesMock.route("MAD", "LGW"),
            RoutesMock.route("MAD", "FCI"),
            RoutesMock.route("IBZ", "BCN"),
            RoutesMock.route("LGW", "BCN"),
            RoutesMock.route("ORY", "BCN"),
            RoutesMock.route("DUB", "BCN"),
            RoutesMock.route("FCI", "BCN"),
            RoutesMock.route("LGW", "LTN"), RoutesMock.route("LTN", "BCN"),
            RoutesMock.route("CDG", "LTN"),
            RoutesMock.route("MAD", "SXF"), RoutesMock.route("SXF", "DUB"),
            RoutesMock.route("MAD", "JFK"), RoutesMock.route("JFK", "DUB"),
            RoutesMock.route("MAD", "BRU"), RoutesMock.route("BRU", "LGW"),
            RoutesMock.route("MAD", "AMS"), RoutesMock.route("AMS", "BRU"),
            RoutesMock.route("BRU", "BCN")
        ));

    // Given
    String departure = "MAD";
    String arrival = "BCN";

    // When
    List<RoutePath> actual = routePathService.findAll(departure, arrival);

    // Then
    assertAll("all-route-paths-list",
        () -> assertThat(actual).isNotNull(),
        () -> assertThat(actual.stream().anyMatch(routePath -> routePath.getStops() == 0)),
        () -> assertThat(actual.stream().anyMatch(routePath -> routePath.getStops() == 1)),
        () -> assertThat(actual.stream().anyMatch(routePath -> routePath.getStops() == 2)),
        () -> assertThat(actual.stream().allMatch(routePath -> routePath.getDeparture().equals("MAD") && routePath.getArrival().equals("BCN"))).isTrue(),
        () -> assertThat(actual.stream().allMatch(routePath -> routePath.getDeparture().equals(routePath.getConnections().get(0)))).isTrue(),
        () -> assertThat(actual.stream().allMatch(routePath -> routePath.getArrival().equals(routePath.getConnections().get(routePath.getConnections().size() - 1)))).isTrue(),
        () -> assertThat(actual.stream().allMatch(routePath -> routePath.getConnections().size() == new HashSet<>(routePath.getConnections()).size())).isTrue()
    );
  }
}