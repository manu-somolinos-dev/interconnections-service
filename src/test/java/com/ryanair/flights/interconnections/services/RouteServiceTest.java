package com.ryanair.flights.interconnections.services;

import com.ryanair.flights.interconnections.adapters.RouteAdapter;
import com.ryanair.flights.interconnections.entities.Route;
import com.ryanair.flights.interconnections.mocks.RoutesMock;
import com.ryanair.flights.interconnections.services.impl.RouteServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.BDDMockito.given;

@ExtendWith(SpringExtension.class)
@DisplayName("route service test case")
public class RouteServiceTest {

  @MockBean
  private RouteAdapter routeAdapter;

  private RouteService routeService;

  @BeforeEach
  public void setUp() {
    // set up
    routeService = new RouteServiceImpl(routeAdapter, asList("RYANAIR"));
  }

  @Test
  @DisplayName("given some mock remote route entities when request routes then return them filtered")
  public void test_find_all() {
    // Mocks
    given(routeAdapter.findAll()).willReturn(RoutesMock.twoStopRoutes("MAD", "BCN"));

    // Given-When
    List<Route> actual = routeService.findAll();

    // Then
    assertAll("find-all-routes-valid",
        () -> assertThat(actual).isNotEmpty(),
        () -> assertThat(actual.stream().allMatch(route -> route.getConnectingAirport() == null && route.getOperator().equals("RYANAIR"))).isTrue()
    );
  }

  @Test
  @DisplayName("given some mock remote route entities and some input criteria when request routes then return them filtered")
  public void test_find_by_airport_from_and_airport_to_not_in() {
    // Mocks
    given(routeAdapter.findAll()).willReturn(RoutesMock.twoStopRoutes("MAD", "BCN"));

    // Given
    String departure = "MAD";
    List<String> arrivals = asList("MAD", "IBZ");

    // When
    List<Route> actual = routeService.findByAirportFromAndAirportToNotIn(departure, arrivals);

    // Then
    assertAll("find-all-routes-valid",
        () -> assertThat(actual).isNotEmpty(),
        () -> assertThat(actual.stream().allMatch(route -> route.getConnectingAirport() == null && route.getOperator().equals("RYANAIR"))).isTrue(),
        () -> assertThat(actual).allMatch(route -> route.getAirportFrom().equals(departure)),
        () -> assertThat(actual).noneMatch(route -> arrivals.contains(route.getAirportTo()))
    );
  }
}