package com.ryanair.flights.interconnections.services;

import com.ryanair.flights.interconnections.domain.Interconnection;
import com.ryanair.flights.interconnections.domain.Leg;
import com.ryanair.flights.interconnections.entities.FlightSchedule;
import com.ryanair.flights.interconnections.entities.RoutePath;
import com.ryanair.flights.interconnections.exception.BadRequestException;
import com.ryanair.flights.interconnections.mocks.RoutePathMocks;
import com.ryanair.flights.interconnections.services.impl.InterconnectionServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

import static com.ryanair.flights.interconnections.mocks.SchedulesMocks.flightSchedule;
import static java.util.Arrays.asList;
import static java.util.Optional.empty;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
@DisplayName("interconnections services test case")
public class InterconnectionServiceTest {

  private InterconnectionService interconnectionService;

  @MockBean
  private ScheduleService scheduleService;

  @MockBean
  private RoutePathService routePathService;

  @BeforeEach
  public void setUp() {
    interconnectionService = new InterconnectionServiceImpl(routePathService, scheduleService, 1, 2);
  }

  @Test
  @DisplayName("given inconsistent input params when a find all interconnections then throws a BadRequest exception")
  public void test_find_all_interconnections_with_inconsistent_input_params() {
    // Given
    String departure = "MAD";
    String arrival = "BCN";
    LocalDateTime departureDate = LocalDateTime.now();
    LocalDateTime arrivalDate = LocalDateTime.now().minus(1, ChronoUnit.DAYS);

    // When-Then
    assertThrows(BadRequestException.class, () -> interconnectionService.findAll(departure, arrival, departureDate, arrivalDate));
  }

  @Test
  @DisplayName("given input filter parameters and a routes list without schedule when find all interconnections then return empty list")
  public void test_find_all_interconnections_for_some_route_paths_any_schedule_found() {
    // Mocks
    given(routePathService.findAll(anyString(), anyString()))
        .willReturn(RoutePathMocks.emptyConnectionsRoutePath());

    given(scheduleService.findFlightSchedule(anyString(), anyString(), any(LocalDateTime.class), any(LocalDateTime.class)))
        .willReturn(empty());

    // Given
    String departure = "MAD";
    String arrival = "BCN";
    LocalDateTime departureDate = LocalDateTime.now();
    LocalDateTime arrivalDate = LocalDateTime.now().plusHours(8);

    // When
    List<Interconnection> actual = interconnectionService.findAll(departure, arrival, departureDate, arrivalDate);

    // Then
    assertAll("empty interconnection list",
        () -> assertThat(actual).isNotNull(),
        () -> assertThat(actual).isEmpty()
    );

    // And
    verify(routePathService, times(1)).findAll(eq(departure), eq(arrival));
    verify(scheduleService, times(1)).findFlightSchedule(eq(departure), eq(arrival), eq(departureDate), eq(arrivalDate));
  }

  @Test
  @DisplayName("given input filter parameters and a routes list with schedule when find all interconnections then return some values")
  public void test_find_all_interconnections_for_some_route_paths_some_schedules_found() {
    // utils
    LocalDateTime today = LocalDateTime.now().withHour(12).withMinute(30).withSecond(0).withNano(0);

    // Mocks
    given(routePathService.findAll(anyString(), anyString()))
        .willReturn(asList(new RoutePath("MAD", "BCN", asList("MAD", "IBZ", "BCN"))));

    FlightSchedule firstFlightSchedule = flightSchedule(LocalTime.of(18, 30), LocalTime.of(21, 00));
    FlightSchedule secondFlightSchedule = flightSchedule(LocalTime.of(23, 30), LocalTime.of(1, 45));

    Mockito.when(scheduleService.findFlightSchedule(eq("MAD"), eq("IBZ"), any(LocalDateTime.class), any(LocalDateTime.class)))
        .thenReturn(Optional.of(firstFlightSchedule));
    Mockito.when(scheduleService.findFlightSchedule(eq("IBZ"), eq("BCN"), any(LocalDateTime.class), any(LocalDateTime.class)))
        .thenReturn(Optional.of(secondFlightSchedule));

    // Given
    String departure = "MAD";
    String arrival = "BCN";
    LocalDateTime departureDate = today;
    LocalDateTime arrivalDate = today.plusDays(1);

    // When
    List<Interconnection> actual = interconnectionService.findAll(departure, arrival, departureDate, arrivalDate);

    // Then
    assertAll("not empty interconnection list",
        () -> assertThat(actual).isNotNull(),
        () -> assertThat(actual).isNotEmpty(),
        () -> assertThat(actual.size()).isEqualTo(1),
        () -> assertThat(actual.get(0).getStops()).isEqualTo(1),
        () -> assertThat(actual.get(0).getLegs()).containsAnyOf(
            new Leg("MAD", "IBZ", today.withHour(18).withMinute(30), today.withHour(21).withMinute(00)),
            new Leg("IBZ", "BCN", today.withHour(23).withMinute(30), today.plusDays(1).withHour(1).withMinute(45))
        )
    );

    // And
    verify(routePathService, times(1)).findAll(eq(departure), eq(arrival));
    verify(scheduleService, times(1)).findFlightSchedule(eq(departure), eq("IBZ"), eq(departureDate), eq(arrivalDate));
    verify(scheduleService, times(1)).findFlightSchedule(eq("IBZ"), eq(arrival), eq(today.withHour(23).withMinute(00)), eq(arrivalDate));
  }

}
