package com.ryanair.flights.interconnections.services;

import com.ryanair.flights.interconnections.adapters.ScheduleAdapter;
import com.ryanair.flights.interconnections.entities.FlightSchedule;
import com.ryanair.flights.interconnections.exception.NotFoundException;
import com.ryanair.flights.interconnections.mocks.SchedulesMocks;
import com.ryanair.flights.interconnections.services.impl.ScheduleServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Optional;

import static com.ryanair.flights.interconnections.mocks.SchedulesMocks.daySchedule;
import static com.ryanair.flights.interconnections.mocks.SchedulesMocks.flightSchedule;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@DisplayName("schedule service bean test case")
@ExtendWith(SpringExtension.class)
public class ScheduleServiceTest {

  @MockBean
  private ScheduleAdapter scheduleAdapter;

  private ScheduleService scheduleService;

  @BeforeEach
  public void setUp() {
    scheduleService = new ScheduleServiceImpl(scheduleAdapter);
  }

  @Test
  @DisplayName("given departure, arrival and a date interval with any remote schedule when check flight schedule then return empty")
  public void test_find_flight_schedule_not_found() {
    // Mocks
    doThrow(NotFoundException.class).when(scheduleAdapter).find(anyString(), anyString(), anyInt(), anyInt());

    // Given
    String departure = "MAD";
    String arrival = "BCN";
    LocalDateTime departureDateTime = LocalDateTime.now();
    LocalDateTime arrivalDateTime = LocalDateTime.now().plusDays(2);

    // When
    Optional<FlightSchedule> actual = scheduleService.findFlightSchedule(departure, arrival, departureDateTime, arrivalDateTime);

    // Then
    assertAll("not found remote schedule",
        () -> assertThat(actual.isEmpty()));
  }

  @Test
  @DisplayName("given departure, arrival and a date interval with some remote schedule when check flight schedule then return some schedule")
  public void test_find_flight_schedule_some_schedule() {
    // Given
    LocalDateTime today = LocalDateTime.now().withHour(10).withMinute(0).withSecond(0).withNano(0);
    String departure = "MAD";
    String arrival = "BCN";
    int month = today.getMonth().getValue();
    int day = today.getDayOfMonth();

    // Mocks
    when(scheduleAdapter.find(anyString(), anyString(), anyInt(), anyInt()))
        .thenReturn(SchedulesMocks.schedule(departure, arrival, month,
            daySchedule(day, flightSchedule(LocalTime.of(20, 0), LocalTime.of(23, 30))),
            daySchedule(day, flightSchedule(LocalTime.of(17, 0), LocalTime.of(19, 30))),
            daySchedule(day, flightSchedule(LocalTime.of(14, 0), LocalTime.of(16, 30))),
            daySchedule(day, flightSchedule(LocalTime.of(11, 0), LocalTime.of(13, 30))),
            daySchedule(day, flightSchedule(LocalTime.of(8, 0), LocalTime.of(11, 30)))
        ));

    // Given
    LocalDateTime departureDateTime = today;
    LocalDateTime arrivalDateTime = today.plusDays(1).withHour(2).withMinute(30);

    // When
    Optional<FlightSchedule> actual = scheduleService.findFlightSchedule(departure, arrival, departureDateTime, arrivalDateTime);

    // Then
    assertAll("found remote schedule - 11am",
        () -> assertThat(actual.isPresent()),
        () -> assertThat(actual.get().getDepartureTime()).isEqualTo(LocalTime.of(11, 0)),
        () -> assertThat(actual.get().getArrivalTime()).isEqualTo(LocalTime.of(13, 30))
    );
  }

  @Test
  @DisplayName("given departure, arrival and a date interval with some remote schedule with flights during midnight when check flight schedule then return some schedule")
  public void test_find_flight_schedule_some_schedule_with_flight_during_midnight() {
    // Given
    LocalDateTime today = LocalDateTime.now().withHour(8).withMinute(0).withSecond(0).withNano(0);
    String departure = "MAD";
    String arrival = "BCN";
    int month = today.getMonth().getValue();
    int day = today.getDayOfMonth();
    LocalTime departureTime = LocalTime.of(23, 30);
    LocalTime arrivalTime = LocalTime.of(1, 30);

    // Mocks
    when(scheduleAdapter.find(anyString(), anyString(), anyInt(), anyInt()))
      .thenReturn(SchedulesMocks.schedule(
          departure, arrival, month, daySchedule(day, flightSchedule(departureTime, arrivalTime))
      ));

    // Given
    LocalDateTime departureDateTime = today;
    LocalDateTime arrivalDateTime = today.plusDays(1).withHour(2).withMinute(30);

    // When
    Optional<FlightSchedule> actual = scheduleService.findFlightSchedule(departure, arrival, departureDateTime, arrivalDateTime);

    // Then
    assertAll("found remote schedule - even with flight during midnight",
        () -> assertThat(actual.isPresent()));
  }
}
