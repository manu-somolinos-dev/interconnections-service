package com.ryanair.flights.interconnections.utils;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("date utilities test case")
public class DateUtilsTest {

  @Test
  @DisplayName("given a localdatetime like 'now at 23:30' when increase two hours then day is increased also")
  public void test_plus_two_hours_increase_day_also() {
    // Given
    LocalDateTime now = LocalDateTime.now().withHour(23).withMinute(30).withSecond(0).withNano(0);

    // When
    LocalDateTime actual = now.plusHours(2);

    // Then
    assertThat(actual.getDayOfMonth()).isNotEqualTo(now.getDayOfMonth());
  }
}
