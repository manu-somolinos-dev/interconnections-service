package com.ryanair.flights.interconnections.actuator;

import com.ryanair.flights.interconnections.adapters.BaseAdapter;
import com.ryanair.flights.interconnections.exception.HealthException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.AbstractMap;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.springframework.boot.actuate.health.Status.DOWN;
import static org.springframework.boot.actuate.health.Status.UP;

@ExtendWith(SpringExtension.class)
@DisplayName("health indicator test case")
public class RemoteProxyHealthIndicatorTest {

  @MockBean
  public BaseAdapter baseAdapter;

  private RemoteProxyHealthIndicator healthIndicator;

  @BeforeEach
  public void setUp() {
    healthIndicator = new RemoteProxyHealthIndicator(baseAdapter, "http://service:8080/health");
  }

  @Test
  @DisplayName("given healthy proxy adapter when check health then is up")
  public void health_up() {
    // Mocks
    doNothing().when(baseAdapter).health();
    // Given
    Health actual = healthIndicator.health();
    // Then
    assertAll("health-up",
        () -> assertThat(actual.getStatus()).isEqualTo(UP),
        () -> assertThat(actual.getDetails()).contains(new AbstractMap.SimpleEntry<>("http://service:8080/health", "UP"))
    );
  }

  @Test
  @DisplayName("given healthy proxy adapter when check health then is down")
  public void health_down() {
    // Mocks
    doThrow(new HealthException("Remote is not Healthy")).when(baseAdapter).health();
    String errorMessage = "com.ryanair.flights.interconnections.exception.HealthException: Remote is not Healthy";
    // Given
    Health actual = healthIndicator.health();
    // Then
    assertAll("health-down",
        () -> assertThat(actual.getStatus()).isEqualTo(DOWN),
        () -> assertThat(actual.getDetails()).contains(new AbstractMap.SimpleEntry<>("http://service:8080/health", "DOWN")),
        () -> assertThat(actual.getDetails()).contains(new AbstractMap.SimpleEntry<>("error", errorMessage))
    );
  }
}