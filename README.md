# Flight Interconnection Search Engine Service

## Introduction
This is a microservice to search flight interconnections using Ryanair routes and schedule api services

## Purpose
The purpose of this repository is allocate a Ryanair code test to build a Flight Interconnection Search Engine Service

## Author

| Name           | Contact               |
| -------------- |-----------------------|
| Manu Somolinos | msomolinosr@gmail.com |

## Contents

* Java microservice with next features:
    * Developed using java jdk-11
    * Gradle integration tool
    * Spring Boot 2:
        * Actuator module
        * Swagger2 module
        * Caching feature
        * Hystrix - Circuit Breaker feature
    * Tests using Junit5
    * Docker configuration:
        * Dockerfile files 
        * Docker-Compose files
        * Kubernetes deployment file
    * Postman REST request project configuration
    * JMeter testing results

## REST API

### Interconnection Endpoints

##### Get Interconnections

```http
GET /flights/interconnections?departure=DUB&arrival=WRO&departureDateTime=2019-10-10T08:00&arrivalDateTime=2019-10-12T23:30

Response Status: 200
Response Body:
[
    {
        "stops": 1,
        "legs": [
            {
                "departureAirport": "DUB",
                "arrivalAirport": "NAP",
                "departureDateTime": "2019-10-10T18:10:00",
                "arrivalDateTime": "2019-10-10T22:25:00"
            },
            {
                "departureAirport": "NAP",
                "arrivalAirport": "WRO",
                "departureDateTime": "2019-10-11T16:55:00",
                "arrivalDateTime": "2019-10-11T19:05:00"
            }
        ]
    },
    {
        "stops": 1,
        "legs": [
            {
                "departureAirport": "DUB",
                "arrivalAirport": "NCL",
                "departureDateTime": "2019-10-10T09:45:00",
                "arrivalDateTime": "2019-10-10T10:55:00"
            },
            {
                "departureAirport": "NCL",
                "arrivalAirport": "WRO",
                "departureDateTime": "2019-10-10T19:30:00",
                "arrivalDateTime": "2019-10-10T22:45:00"
            }
        ]
    },
    {
        "stops": 0,
        "legs": [
            {
                "departureAirport": "DUB",
                "arrivalAirport": "WRO",
                "departureDateTime": "2019-10-10T19:30:00",
                "arrivalDateTime": "2019-10-10T23:05:00"
            }
        ]
    }
]
```